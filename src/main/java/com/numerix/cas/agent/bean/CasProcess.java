package com.numerix.cas.agent.bean;

import com.numerix.cas.agent.enums.ProcessType;
import com.numerix.cas.exception.CASException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.beans.ConstructorProperties;
import java.util.Date;

@Getter
@ToString
@EqualsAndHashCode(exclude = {"type", "casProcessDetails"})
public class CasProcess {

    private final String name;
    private final ProcessType type;
    private final CasProcessDetails casProcessDetails;

    @ConstructorProperties({"name"})
    public CasProcess(String name) throws CASException {
        this.name = name;
        this.type = getProcessType(name);
        this.casProcessDetails = new CasProcessDetails(name);
    }

    private ProcessType getProcessType(String name) throws CASException {
        return ProcessType.getProcessType(name.replaceAll("[0-9]*$", ""));
    }

    @Getter
    @ToString
    public class CasProcessDetails {
        private final String name;
        @Setter
        private int pid;
        @Setter
        private int childPid;
        @Setter
        private long memoryUsed;
        @Setter
        private Date startTime;
        @Setter
        private boolean running;

        public CasProcessDetails(String name) {
            this.name = name;
        }
    }
}
