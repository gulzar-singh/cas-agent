package com.numerix.cas.agent.controller;

import com.numerix.cas.agent.operation.ProcessLogOperation;
import com.numerix.cas.agent.util.CasAgentConstants;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
@RestController
@RequestMapping("/log")
public class ProcessLogController {

    @Autowired
    private ProcessLogOperation processLogOperation;

    @Value(CasAgentConstants.LOG_ZIP_FILE)
    private String logZipFile;

    @RequestMapping(method = RequestMethod.GET)
    public void getProcessLogZipFile(HttpServletResponse response) throws CASException {
        try (ByteArrayOutputStream byteArrayOutputStream = processLogOperation.getProcessLogZipFile()) {
            response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment; filename=" + logZipFile);
            response.getOutputStream().write(byteArrayOutputStream.toByteArray());
            response.flushBuffer();
        } catch (IOException e) {
            throw new CASException(CrossAssetServerErrorCode.IOError, e);
        }
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public void getProcessLog(HttpServletResponse response, @PathVariable("name") String name, @RequestParam(value = "pagesize", required = false) Integer pageSize, @RequestParam(value = "pageno", required = false) Integer pageNo) throws CASException {
        if (pageSize == null) {
            writeProcessLogFileToResponse(response, StringUtils.upperCase(name));
            return;
        }
        writeProcessLogTailToResponse(response, StringUtils.upperCase(name), pageSize, pageNo);
    }

    private void writeProcessLogFileToResponse(HttpServletResponse response, String name) throws CASException {
        try (InputStream inputStream = processLogOperation.getProcessLogFile(name)) {
            String fileName = name + ".log";
            response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new CASException(CrossAssetServerErrorCode.IOError, e);
        }
    }

    private void writeProcessLogTailToResponse(HttpServletResponse response, String name, Integer pageSize, Integer pageNo) throws CASException {
        try {
            response.getOutputStream().write(processLogOperation.getProcessLogTail(name, pageSize, pageNo).getBytes());
            response.flushBuffer();
        } catch (IOException e) {
            throw new CASException(CrossAssetServerErrorCode.IOError, e);
        }
    }
}
