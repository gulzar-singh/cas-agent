package com.numerix.cas.agent.controller;

import com.numerix.cas.agent.bean.CasProcess;
import com.numerix.cas.agent.operation.ProcessOperation;
import com.numerix.cas.exception.CASException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/process")
public class ProcessController {

    @Autowired
    private ProcessOperation processOperation;

    @RequestMapping(method = RequestMethod.GET)
    public List<CasProcess.CasProcessDetails> getAll() throws CASException {
        return processOperation.getProcess();
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public CasProcess.CasProcessDetails getProcess(@PathVariable("name") String name) throws CASException {
        return processOperation.getProcess(StringUtils.upperCase(name));
    }

    @RequestMapping(value = "/{name}", method = {RequestMethod.POST})
    public CasProcess.CasProcessDetails startProcess(@PathVariable("name") String name) throws CASException {
        return processOperation.startProcess(StringUtils.upperCase(name));
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.DELETE)
    public CasProcess.CasProcessDetails stopProcess(@PathVariable("name") String name) throws CASException {
        return processOperation.stopProcess(StringUtils.upperCase(name));
    }
}