package com.numerix.cas.agent.startup.impl;

import com.numerix.cas.agent.bean.CasProcess;
import com.numerix.cas.agent.enums.ProcessType;
import com.numerix.cas.agent.startup.CasProcessInitializer;
import com.numerix.cas.agent.store.CasAgentDataStore;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import com.numerix.cas.service.config.ControlService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class CasProcessInitializerImpl implements CasProcessInitializer {

    @Autowired
    private CasAgentDataStore casAgentDataStore;

    @Override
    public void initialize() throws CASException {
        log.info("Initializing cas processes");
        Map<ProcessType, ControlService.Application> controlServiceConfigMap = casAgentDataStore.getControlServiceConfigMap();
        if (MapUtils.isEmpty(controlServiceConfigMap)) {
            throw new CASException(CrossAssetServerErrorCode.NullArgumentError, "No control config found.");
        }
        for (Map.Entry<ProcessType, ControlService.Application> entry : controlServiceConfigMap.entrySet()) {
            Map<String, CasProcess> casProcessMap = createCasProcessMap(entry);
            if (MapUtils.isNotEmpty(casProcessMap)) {
                casAgentDataStore.getCasProcessMap().putAll(casProcessMap);
            }
        }
    }

    private Map<String, CasProcess> createCasProcessMap(Map.Entry<ProcessType, ControlService.Application> entry) throws CASException {
        ControlService.Application application = entry.getValue();
        if (application.getCount() <= 0) {
            return Collections.emptyMap();
        }
        boolean singleProcess = application.getCount() == 1;
        if (singleProcess) {
            CasProcess casProcess = new CasProcess(entry.getKey().name());
            log.info("Initialized CasProcess: {}", casProcess);
            return Collections.singletonMap(entry.getKey().name(), casProcess);
        }
        Map<String, CasProcess> casProcessMap = new ConcurrentHashMap<>();
        for (int i = 1; i <= application.getCount(); i++) {
            String name = entry.getKey().name() + i;
            CasProcess casProcess = new CasProcess(name);
            log.info("Initialized CasProcess: {}", casProcess);
            casProcessMap.put(name, casProcess);
        }

        return casProcessMap;
    }
}
