package com.numerix.cas.agent.startup.impl;

import com.numerix.cas.agent.bean.CasProcess;
import com.numerix.cas.agent.enums.ProcessType;
import com.numerix.cas.agent.operation.ProcessInitOperation;
import com.numerix.cas.agent.startup.CasProcessStarter;
import com.numerix.cas.agent.store.CasAgentDataStore;
import com.numerix.cas.agent.util.CasAgentConstants;
import com.numerix.cas.exception.CASException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CasProcessStarterImpl implements CasProcessStarter {

    @Autowired
    private CasAgentDataStore casAgentDataStore;

    @Autowired
    private ProcessInitOperation processInitOperation;

    @Value(CasAgentConstants.STARTUP_DELAY)
    private long startupDelay;

    @Override
    public void start() throws CASException {
        log.info("Starting cas processes");
        startBrokerCasProcess(); //Started broker as first process to start other processes normally.
        for (CasProcess casProcess : casAgentDataStore.getCasProcessMap().values()) {
            if (ProcessType.BROKER == casProcess.getType()) {
                continue;   //Already started above
            }

            processInitOperation.startCasProcess(casProcess);
            log.info("Started {}", casProcess);
        }
    }

    private void startBrokerCasProcess() throws CASException {
        CasProcess casProcess = processInitOperation.startCasProcess(casAgentDataStore.getCasProcessMap().get(ProcessType.BROKER.name()));//Assuming 1 Broker will be started.
        log.info("Started {}", casProcess);
        try {
            Thread.sleep(startupDelay);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
    }
}
