package com.numerix.cas.agent.startup;

import com.numerix.cas.exception.CASException;

public interface CasProcessStarter {
    void start() throws CASException;
}
