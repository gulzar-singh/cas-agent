package com.numerix.cas.agent.startup;

import com.numerix.cas.exception.CASException;

public interface AgentConfigLoader {
    void load() throws CASException;
}
