package com.numerix.cas.agent.startup.impl;

import com.numerix.cas.agent.enums.ProcessType;
import com.numerix.cas.agent.startup.ControlServiceConfigLoader;
import com.numerix.cas.agent.store.CasAgentDataStore;
import com.numerix.cas.agent.util.CasAgentConstants;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import com.numerix.cas.service.config.ControlService;
import com.numerix.cas.xml.CASXMLUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Paths;

@Slf4j
@Component
public class ControlServiceConfigLoaderImpl implements ControlServiceConfigLoader {

    @Value(CasAgentConstants.ENV)
    private String env;

    @Autowired
    private CasAgentDataStore casAgentDataStore;

    @Override
    public void load() throws CASException {
        log.info("Loading cas-server_control.xml..");
        CASXMLUtils xmlUtil = new CASXMLUtils(ControlService.class, ControlService.class.getPackage().getName(), "xsd/ServiceConfiguration.xsd");
        try {
            ControlService controlService = (ControlService) xmlUtil.unmarshalFromStream(getInputStream());
            log.info("cas-server_control.xml Configuration successfully unmarshalled.");
            for (ControlService.Application app : controlService.getApplication()) {
                casAgentDataStore.getControlServiceConfigMap().put(ProcessType.getProcessType(app.getLabel()), app);
            }
        } catch (CASException e) {
            throw e;
        } catch (Exception e) {
            throw new CASException(CrossAssetServerErrorCode.ProcessXMLError, e);
        }
    }

    private InputStream getInputStream() throws FileNotFoundException {
        return new FileInputStream(Paths.get(casAgentDataStore.getCasBaseDir(), "conf", "cas-server_control.xml").toFile());
    }
}
