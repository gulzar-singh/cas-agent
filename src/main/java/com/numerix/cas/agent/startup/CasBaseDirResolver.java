package com.numerix.cas.agent.startup;

public interface CasBaseDirResolver {
    void load();
}