package com.numerix.cas.agent.startup;

import com.numerix.cas.agent.util.ApplicationContextUtils;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.exception.CASRuntimeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AgentApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     *
     * @param event application ready event
     */
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        log.info("Running Agent application Startup");
        try {
            ConfigurableApplicationContext context = event.getApplicationContext();
            ApplicationContextUtils.getInstance().setApplicationContext(context);
            context.getBean(AgentConfigLoader.class).load();
            context.getBean(CasProcessStarter.class).start();
        } catch (CASException e) {
            log.error("CAS:{}:{} - {}", e.getErrorCode().getCode(), e.getErrorCode().getName(), e.getMessage(), e);
            throw new CASRuntimeException(e.getErrorCode(), e.getMessage(), e);
        }
    }
}