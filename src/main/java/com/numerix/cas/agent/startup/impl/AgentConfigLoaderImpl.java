package com.numerix.cas.agent.startup.impl;

import com.numerix.cas.agent.startup.AgentConfigLoader;
import com.numerix.cas.agent.startup.CasBaseDirResolver;
import com.numerix.cas.agent.startup.CasProcessInitializer;
import com.numerix.cas.agent.startup.ControlServiceConfigLoader;
import com.numerix.cas.exception.CASException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AgentConfigLoaderImpl implements AgentConfigLoader {

    @Autowired
    private CasBaseDirResolver casBaseDirResolver;

    @Autowired
    private ControlServiceConfigLoader controlServiceConfigLoader;

    @Autowired
    private CasProcessInitializer casProcessInitializer;

    @Override
    public void load() throws CASException {
        casBaseDirResolver.load();
        controlServiceConfigLoader.load();
        casProcessInitializer.initialize();
    }
}
