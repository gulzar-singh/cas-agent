package com.numerix.cas.agent.startup.impl;

import com.numerix.cas.agent.startup.CasBaseDirResolver;
import com.numerix.cas.agent.store.CasAgentDataStore;
import com.numerix.cas.agent.util.CasAgentConstants;
import com.numerix.cas.exception.CASRuntimeException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Component
public class CasBaseDirResolverImpl implements CasBaseDirResolver {

    @Autowired
    private CasAgentDataStore casAgentDataStore;

    @Override
    public void load() {
        String casBaseDir = System.getProperty(CasAgentConstants.CAS_DIR);
        if (StringUtils.isBlank(casBaseDir)) {
            try {
                Path path = Paths.get(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
                if (path.toString().endsWith("classes")) {
                    throw new CASRuntimeException(CrossAssetServerErrorCode.IOError, "cas.dir not set. Unable to resolve cas base dir.");
                } else if (path.toString().endsWith(".jar")) {
                    casAgentDataStore.setCasBaseDir(path.getParent().getParent().toString());
                } else {
                    throw new CASRuntimeException(CrossAssetServerErrorCode.IOError, "Invalid cas base dir : " + path.toString());
                }
            } catch (URISyntaxException e) {
                throw new CASRuntimeException(CrossAssetServerErrorCode.IOError, e);
            }
        } else {
            casAgentDataStore.setCasBaseDir(casBaseDir);
        }
        log.info("Cas Base Dir: {}", casAgentDataStore.getCasBaseDir());
    }
}
