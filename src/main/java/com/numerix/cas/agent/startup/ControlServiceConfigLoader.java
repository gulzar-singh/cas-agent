package com.numerix.cas.agent.startup;

import com.numerix.cas.exception.CASException;

public interface ControlServiceConfigLoader {
    void load() throws CASException;
}
