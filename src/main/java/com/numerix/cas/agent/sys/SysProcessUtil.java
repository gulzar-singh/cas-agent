package com.numerix.cas.agent.sys;

import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import com.sun.jna.Platform;
import org.zeroturnaround.exec.ProcessExecutor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class SysProcessUtil {
    private SysProcessUtil() {
    }

    public static List<Integer> getJavaPids() throws CASException {
        if (Platform.isWindows()) {
            return getWindowsJavaPids();
        }
        return getUnixJavaPids();
    }

    private static List<Integer> getWindowsJavaPids() throws CASException {
        try {
            String output = new ProcessExecutor()
                    .command("tasklist", "/fi", "\"imagename eq java.exe\"", "/fo", "csv", "/nh")
                    .readOutput(true)
                    .execute().outputUTF8();
            List<Integer> pids = new ArrayList<>();
            for (String line : output.split(System.lineSeparator())) {
                pids.add(Integer.parseInt(line.split("\",\"")[1]));
            }
            return pids;
        } catch (Exception e) {
            throw new CASException(CrossAssetServerErrorCode.GeneralError, e);
        }
    }

    private static List<Integer> getUnixJavaPids() throws CASException {
        try {
            String output = new ProcessExecutor()
                    .command("pgrep", "java")
                    .readOutput(true)
                    .execute().outputUTF8();
            List<Integer> pids = new ArrayList<>();
            for (String line : output.split(System.lineSeparator())) {
                pids.add(Integer.parseInt(line));
            }
            return pids;
        } catch (Exception e) {
            throw new CASException(CrossAssetServerErrorCode.GeneralError, e);
        }
    }

    public static Integer diffJavaPid(List<Integer> oldPids, List<Integer> newPids) {
        List<Integer> diff = new ArrayList<>();
        for (Integer pid : newPids) {
            if (oldPids.contains(pid)) {
                continue;
            }
            diff.add(pid);
        }
        if (diff.size() == 1) {
            return diff.get(0);
        }
        return null;
    }
}
