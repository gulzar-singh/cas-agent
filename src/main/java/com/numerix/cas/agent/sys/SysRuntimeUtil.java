package com.numerix.cas.agent.sys;

import com.sun.jna.Platform;
import com.sun.management.OperatingSystemMXBean;

import java.lang.management.ManagementFactory;

public class SysRuntimeUtil {

    private SysRuntimeUtil() {
    }

    public static boolean isWindows() {
        return Platform.isWindows();
    }

    public static long getOSFreeMemory() {
        OperatingSystemMXBean bean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        return bean.getFreePhysicalMemorySize();
    }
}
