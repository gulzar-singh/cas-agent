package com.numerix.cas.agent;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@Slf4j
@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = {"com.numerix.cas.agent"}, lazyInit = true)
public class LaunchAgentApplication {
    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            log.error("No valid arguments passed.");
            return;
        }
        if ("start".equals(args[0])) {
            start(args);
        } else if ("stop".equals(args[0])) {
            stop(args);
        } else {
            log.error("Must supply the argument 'start' or 'stop'");
        }
    }

    public static void start(String[] args) {
        log.info("==============================================");
        log.info(" Starting Numerix Cas Agent Service..");
        SpringApplication.run(LaunchAgentApplication.class, args);
    }

    public static void stop(String[] args) {
        log.info(" Exiting System.");
        log.info("===========================================");
        System.exit(0);
    }
}