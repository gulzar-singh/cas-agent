package com.numerix.cas.agent.operation.impl;

import com.numerix.cas.agent.bean.CasProcess;
import com.numerix.cas.agent.operation.ProcessInitOperation;
import com.numerix.cas.agent.store.CasAgentDataStore;
import com.numerix.cas.agent.sys.SysProcessUtil;
import com.numerix.cas.agent.sys.SysRuntimeUtil;
import com.numerix.cas.agent.util.CasAgentProcessLogger;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import com.numerix.cas.service.config.ControlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zeroturnaround.process.PidProcess;
import org.zeroturnaround.process.Processes;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;

@Slf4j
@Component
public class ProcessInitOperationImpl implements ProcessInitOperation {

    @Autowired
    private CasAgentDataStore casAgentDataStore;

    @Override
    public CasProcess startCasProcess(CasProcess casProcess) throws CASException {
        ControlService.Application application = casAgentDataStore.getControlServiceConfigMap().get(casProcess.getType());
        checkProcessStartMemoryLimit(casProcess.getName(), application.getRestartMemorySize());
        try {
            List<Integer> oldPids = SysProcessUtil.getJavaPids();
            Process sysProcess = startSysProcess(application);
            casAgentDataStore.getProcessMap().put(casProcess.getName(), sysProcess);
            new CasAgentProcessLogger(casProcess.getName(), sysProcess.getInputStream()).start();
            populateStartCasProcess(casProcess, Processes.newPidProcess(sysProcess).getPid(), SysProcessUtil.diffJavaPid(oldPids, SysProcessUtil.getJavaPids()));
            return casProcess;
        } catch (IOException e) {
            throw new CASException(CrossAssetServerErrorCode.IOError, e);
        }
    }

    @Override
    public CasProcess stopCasProcess(CasProcess casProcess) throws CASException {
        Process process = casAgentDataStore.getProcessMap().get(casProcess.getName());
        PidProcess pidProcess = Processes.newPidProcess(process);
        try {
            pidProcess.destroy(true);
            PidProcess childPidProcess = Processes.newPidProcess(casProcess.getCasProcessDetails().getChildPid());
            if(childPidProcess.isAlive()) {
                log.debug("Cas Process: {} - Child PID is still running. Attempting to kill it.", casProcess);
                childPidProcess.destroy(true);
            }
            casAgentDataStore.getProcessMap().remove(casProcess.getName());
            populateStopCasProcess(casProcess);
        } catch (IOException e) {
            throw new CASException(CrossAssetServerErrorCode.IOError, e);
        } catch (InterruptedException e) {
            throw new CASException(CrossAssetServerErrorCode.GeneralError, e);
        }
        return casProcess;
    }

    private CasProcess populateStartCasProcess(CasProcess casProcess, Integer pid, Integer childPid) {
        CasProcess.CasProcessDetails casProcessDetails = casProcess.getCasProcessDetails();
        casProcessDetails.setRunning(true);
        casProcessDetails.setPid(pid);
        if (childPid != null) {
            casProcessDetails.setChildPid(childPid);
        }
        casProcessDetails.setStartTime(Calendar.getInstance().getTime());
        return casProcess;
    }

    private CasProcess populateStopCasProcess(CasProcess casProcess) {
        CasProcess.CasProcessDetails casProcessDetails = casProcess.getCasProcessDetails();
        casProcessDetails.setRunning(false);
        casProcessDetails.setPid(0);
        casProcessDetails.setChildPid(0);
        casProcessDetails.setStartTime(null);
        casProcessDetails.setMemoryUsed(0);
        return casProcess;
    }

    private Process startSysProcess(ControlService.Application application) throws IOException {
        return new ProcessBuilder()
                .command(getAbsoluteStartupPath(application.getStartupScript())/*, "-runFromControlService", "-agentPid", String.valueOf(PidUtil.getMyPid())*/)
                .redirectErrorStream(true)
                .start();
    }

    private String getAbsoluteStartupPath(String startupScript) {
        return Paths.get(casAgentDataStore.getCasBaseDir(), startupScript).toString();
    }

    private void checkProcessStartMemoryLimit(String name, long memoryRequired) throws CASException {
        long memoryAvailable = SysRuntimeUtil.getOSFreeMemory();
        if (memoryAvailable < memoryRequired) {
            throw new CASException(CrossAssetServerErrorCode.InsufficientMemoryError, "Not enough free memory to startup " + name + " Available=" + memoryAvailable + " Required=" + memoryRequired);
        }
    }
}
