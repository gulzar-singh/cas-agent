package com.numerix.cas.agent.operation;

import com.numerix.cas.agent.bean.CasProcess;
import com.numerix.cas.exception.CASException;

import java.util.List;

public interface ProcessOperation {
    List<CasProcess.CasProcessDetails> getProcess();
    CasProcess.CasProcessDetails getProcess(String name) throws CASException;
    CasProcess.CasProcessDetails startProcess(String name) throws CASException;
    CasProcess.CasProcessDetails stopProcess(String name) throws CASException;
}
