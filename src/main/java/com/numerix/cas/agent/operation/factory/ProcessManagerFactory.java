package com.numerix.cas.agent.operation.factory;

import com.numerix.cas.agent.enums.ProcessType;
import com.numerix.cas.agent.operation.ProcessManager;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ProcessManagerFactory {

    @Autowired
    private ApplicationContext applicationContext;

    private Map<String, ProcessManager> instanceMap;

    public ProcessManager processManager(ProcessType processType) throws CASException {
        if (instanceMap == null) {
            instanceMap = new ConcurrentHashMap<>(6);   //Set the size according to process type
        }
        String instanceName = processType + "ProcessManager";
        ProcessManager instance = instanceMap.get(instanceName);
        if (instance == null) {
            return instanceMap.put(instanceName, (ProcessManager) applicationContext.getBean(instanceName));
        } else {

        }
        throw new CASException(CrossAssetServerErrorCode.InvalidArgumentError, "No process type specified");
    }
}