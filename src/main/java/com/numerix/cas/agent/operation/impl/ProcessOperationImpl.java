package com.numerix.cas.agent.operation.impl;

import com.numerix.cas.agent.bean.CasProcess;
import com.numerix.cas.agent.operation.ProcessInitOperation;
import com.numerix.cas.agent.operation.ProcessOperation;
import com.numerix.cas.agent.store.CasAgentDataStore;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class ProcessOperationImpl implements ProcessOperation {

    @Autowired
    private CasAgentDataStore casAgentDataStore;

    @Autowired
    private ProcessInitOperation processInitOperation;

    @Override
    public List<CasProcess.CasProcessDetails> getProcess() {
        return new ArrayList<>(casAgentDataStore.getCasProcessDetails());
    }

    @Override
    public CasProcess.CasProcessDetails getProcess(String name) throws CASException {
        casAgentDataStore.checkIfExist(name);
        return casAgentDataStore.getCasProcessDetails(name);
    }

    @Override
    public CasProcess.CasProcessDetails startProcess(String name) throws CASException {
        casAgentDataStore.checkIfExist(name);
        CasProcess casProcess = casAgentDataStore.getCasProcess(name);
        if (casProcess.getCasProcessDetails().isRunning()) {
            throw new CASException(CrossAssetServerErrorCode.DuplicateError, casProcess.getName() + " is already running");
        }
        log.debug("Cas Process:{} - Attempting to start it.", casProcess);
        processInitOperation.startCasProcess(casProcess);
        log.debug("Cas Process:{} - Started", casProcess);
        return casProcess.getCasProcessDetails();
    }

    @Override
    public CasProcess.CasProcessDetails stopProcess(String name) throws CASException {
        casAgentDataStore.checkIfExist(name);
        CasProcess casProcess = casAgentDataStore.getCasProcess(name);
        if (!casProcess.getCasProcessDetails().isRunning()) {
            throw new CASException(CrossAssetServerErrorCode.InvalidOperationError, "Process:" + name + " isn't running.");
        }
        log.debug("Cas Process:{} - Attempting to stop it.", casProcess);
        processInitOperation.stopCasProcess(casProcess);
        log.debug("Cas Process:{} - Stopped", casProcess);
        return casProcess.getCasProcessDetails();
    }


}
