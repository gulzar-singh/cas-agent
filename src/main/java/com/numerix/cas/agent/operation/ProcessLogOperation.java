package com.numerix.cas.agent.operation;

import com.numerix.cas.exception.CASException;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

public interface ProcessLogOperation {
    ByteArrayOutputStream getProcessLogZipFile() throws CASException;
    InputStream getProcessLogFile(String name) throws CASException;
    String getProcessLogTail(String name, Integer pageSize, Integer pageNo) throws CASException;
}
