package com.numerix.cas.agent.operation;

import com.numerix.cas.agent.bean.CasProcess;

import java.util.List;

public interface ProcessManager {
    List<CasProcess.CasProcessDetails> getAllProcesses();

    CasProcess.CasProcessDetails getProcess(String name);

    List<CasProcess.CasProcessDetails> startProcess(String name);

    List<CasProcess.CasProcessDetails> stopProcess(String name);
}
