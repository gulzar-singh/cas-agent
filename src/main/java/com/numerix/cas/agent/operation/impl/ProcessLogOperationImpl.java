package com.numerix.cas.agent.operation.impl;

import com.numerix.cas.agent.operation.ProcessLogOperation;
import com.numerix.cas.agent.store.CasAgentDataStore;
import com.numerix.cas.agent.util.CasAgentConstants;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.input.ReversedLinesFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
@Component
public class ProcessLogOperationImpl implements ProcessLogOperation {

    @Autowired
    private CasAgentDataStore casAgentDataStore;

    @Value(CasAgentConstants.DEFAULT_LOG_PAGE_SIZE)
    private int defaultPageSize;

    @Value(CasAgentConstants.MAX_LOG_PAGE_SIZE)
    private int maxPageSize;

    @Value(CasAgentConstants.MAX_LOG_PAGE_NO)
    private int maxPageNo;

    @Override
    public ByteArrayOutputStream getProcessLogZipFile() throws CASException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (ZipOutputStream zos = new ZipOutputStream(byteArrayOutputStream)) {
            for (File logFile : FileUtils.listFiles(Paths.get(casAgentDataStore.getCasBaseDir(), "log").toFile(), new String[]{"log"}, false)) {
                ZipEntry ze = new ZipEntry(logFile.getName());
                zos.putNextEntry(ze);
                writeToZipFile(logFile, zos);
                zos.closeEntry();
            }
            return byteArrayOutputStream;
        } catch (IOException e) {
            throw new CASException(CrossAssetServerErrorCode.IOError, e);
        }
    }

    private void writeToZipFile(File logFile, ZipOutputStream zos) throws IOException {
        byte[] buffer = new byte[1024];
        try (FileInputStream in = new FileInputStream(logFile)) {
            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            in.close();
        }
    }

    @Override
    public InputStream getProcessLogFile(String name) throws CASException {
        casAgentDataStore.checkIfExist(name);
        Path logFilePath = Paths.get(casAgentDataStore.getCasBaseDir(), "log", name + ".log");
        try {
            return Files.newInputStream(logFilePath);
        } catch (IOException e) {
            throw new CASException(CrossAssetServerErrorCode.IOError, e);
        }
    }

    @Override
    public String getProcessLogTail(String name, Integer pageSize, Integer pageNo) throws CASException {
        casAgentDataStore.checkIfExist(name);
        Path logFilePath = Paths.get(casAgentDataStore.getCasBaseDir(), "log", name + ".log");
        StringBuilder output = new StringBuilder();
        try (ReversedLinesFileReader fileReader = new ReversedLinesFileReader(logFilePath.toFile())) {
            int startSize = getStartSize(pageSize, pageNo);
            int endSize = getEndSize(startSize, pageSize);
            String line;
            int counter = 0;
            do {
                line = fileReader.readLine();
                if (counter >= startSize) {
                    output.append(line).append(System.lineSeparator());
                }
                if (counter >= endSize) {
                    break;
                }
                counter++;
            } while (line != null);
            return output.toString();
        } catch (IOException e) {
            throw new CASException(CrossAssetServerErrorCode.IOError, e);
        }
    }

    private int getStartSize(Integer pageSize, Integer pageNo) {
        return getValidPageSize(pageSize) * getValidPageNo(pageNo);
    }

    private int getEndSize(int startSize, int pageSize) {
        return startSize + getValidPageSize(pageSize);
    }

    private int getValidPageSize(Integer pageSize) {
        int validPageSize = (pageSize != null && pageSize > 0) ? pageSize : defaultPageSize;
        if (validPageSize > maxPageSize) {
            validPageSize = maxPageSize;
        }
        return validPageSize;
    }

    private int getValidPageNo(Integer pageNo) {
        int validPageNo = (pageNo != null && pageNo > 0) ? pageNo : 0;
        if (validPageNo > maxPageNo) {
            validPageNo = maxPageNo;
        }
        return validPageNo;
    }
}
