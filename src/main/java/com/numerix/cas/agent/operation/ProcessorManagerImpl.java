package com.numerix.cas.agent.operation;


import com.numerix.cas.agent.bean.CasProcess;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class ProcessorManagerImpl implements ProcessManager {

    @Override
    public List<CasProcess.CasProcessDetails> getAllProcesses() {
        return null;
    }

    @Override
    public CasProcess.CasProcessDetails getProcess(String name) {
        return null;
    }

    @Override
    public List<CasProcess.CasProcessDetails> startProcess(String name) {
        return null;
    }

    @Override
    public List<CasProcess.CasProcessDetails> stopProcess(String name) {
        return null;
    }
}
