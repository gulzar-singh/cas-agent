package com.numerix.cas.agent.operation;

import com.numerix.cas.agent.bean.CasProcess;
import com.numerix.cas.exception.CASException;

public interface ProcessInitOperation {
    CasProcess startCasProcess(CasProcess casProcess) throws CASException;
    CasProcess stopCasProcess(CasProcess casProcess) throws CASException;
}
