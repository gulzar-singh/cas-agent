package com.numerix.cas.agent.util;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Slf4j
public class CasAgentProcessLogger extends Thread {

    private String logFile;
    private InputStream in;

    public CasAgentProcessLogger(String logFile, InputStream in) {
        this.logFile = logFile;
        this.in = in;
    }

    @Override
    public void run() {
        try (InputStreamReader isr = new InputStreamReader(in)) {
            MDC.put("logFile", logFile);
            BufferedReader input = new BufferedReader(isr);
            String line = null;
            while ((line = input.readLine()) != null) {
                log.debug(line);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            MDC.remove("logFile");
        }
    }
}
