package com.numerix.cas.agent.util;

public class CasAgentConstants {
    public static final String DEV_IDE = "dev-ide";
    public static final String ENV = "${cas.agent.env:" + DEV_IDE + "}";
    public static final String CAS_DIR = "cas.dir";
    public static final String STARTUP_DELAY = "${cas.agent.startup.delay:1000}";
    public static final String LOG_ZIP_FILE = "${cas.agent.log.zip.file:CASFULL.zip}";
    public static final String DEFAULT_LOG_PAGE_SIZE = "${cas.agent.log.default.page.size:100}";
    public static final String MAX_LOG_PAGE_SIZE = "${cas.agent.log.max.page.size:1000}";
    public static final String MAX_LOG_PAGE_NO = "${cas.agent.log.max.page.no:50}";

    private CasAgentConstants() {
    }
}
