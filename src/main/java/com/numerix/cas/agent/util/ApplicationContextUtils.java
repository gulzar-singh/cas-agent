package com.numerix.cas.agent.util;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationContext;

public class ApplicationContextUtils {
    @Getter
    @Setter
    private ApplicationContext applicationContext;
    private static final ApplicationContextUtils instance = new ApplicationContextUtils();

    private ApplicationContextUtils() {
    }

    public static ApplicationContextUtils getInstance() {
        return instance;
    }
}
