package com.numerix.cas.agent.enums;

import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;

public enum ProcessType {
    BROKER, SERVER, CNODE;

    public static ProcessType getProcessType(String type) throws CASException {
        try {
            return ProcessType.valueOf(type.trim().toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CASException(CrossAssetServerErrorCode.InvalidArgumentError, "No process type defined for " + type);
        }
    }
}
