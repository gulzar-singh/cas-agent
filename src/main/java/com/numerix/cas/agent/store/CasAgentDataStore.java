package com.numerix.cas.agent.store;

import com.numerix.cas.agent.bean.CasProcess;
import com.numerix.cas.agent.enums.ProcessType;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import com.numerix.cas.service.config.ControlService;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static ch.lambdaj.Lambda.extract;
import static ch.lambdaj.Lambda.on;

@Getter
@Component
public class CasAgentDataStore {
    private Map<String, CasProcess> casProcessMap = new ConcurrentHashMap<>();
    private Map<String, Process> processMap = new ConcurrentHashMap<>();
    private Map<ProcessType, ControlService.Application> controlServiceConfigMap = new ConcurrentHashMap<>(5); //Setting the size 5 based on assumption that cas-server_control have 3 entries.

    @Setter
    private String casBaseDir;

    public CasProcess getCasProcess(String name) {
        return casProcessMap.get(name);
    }

    public List<CasProcess.CasProcessDetails> getCasProcessDetails() {
        return extract(casProcessMap.values(), on(CasProcess.class).getCasProcessDetails());
    }

    public CasProcess.CasProcessDetails getCasProcessDetails(String name) {
        return casProcessMap.get(name).getCasProcessDetails();
    }

    public boolean checkIfExist(String name) throws CASException {
        if (StringUtils.isBlank(name) || !(casProcessMap.containsKey(name))) {
            throw new CASException(CrossAssetServerErrorCode.InvalidArgumentError, "Process:" + name + " doesn't exist.");
        }
        return true;
    }
}
