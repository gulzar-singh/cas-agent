package com.numerix.cas.agent.log.factory;

import com.numerix.cas.agent.enums.ProcessType;
import com.numerix.cas.agent.log.ProcessLogManager;
import com.numerix.cas.exception.CASException;
import com.numerix.cas.iface.CrossAssetServerErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class LogManagerFactory {

    @Autowired
    private ApplicationContext applicationContext;

    private Map<String, ProcessLogManager> instanceMap;

    public ProcessLogManager processLogManager(ProcessType processType) throws CASException {
        if (instanceMap == null) {
            instanceMap = new ConcurrentHashMap<>(6);   //Set the size according to process type
        }
        String instanceName = processType + "ProcessLogManager";
        ProcessLogManager instance = instanceMap.get(instanceName);
        if (instance == null) {
            return instanceMap.put(instanceName, (ProcessLogManager) applicationContext.getBean(instanceName));
        }
        throw new CASException(CrossAssetServerErrorCode.InvalidArgumentError, "No process type specified");
    }

}