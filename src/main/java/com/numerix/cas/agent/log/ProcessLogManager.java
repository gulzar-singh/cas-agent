package com.numerix.cas.agent.log;

public interface ProcessLogManager {
    void getAllLogZipFile();

    void getProcessLogFile(String name);

    String getTailProcessLog(String name, int pageSize, int pageNo);
}