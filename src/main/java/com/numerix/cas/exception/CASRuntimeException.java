package com.numerix.cas.exception;

import com.numerix.cas.iface.CrossAssetServerErrorCode;

/**
 * Created by gsingh on 11/04/2016.
 */
public class CASRuntimeException extends RuntimeException {

    private final CrossAssetServerErrorCode errorCode;

    public CASRuntimeException(CrossAssetServerErrorCode errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public CASRuntimeException(CrossAssetServerErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public CASRuntimeException(CrossAssetServerErrorCode errorCode, String message, Throwable t) {
        super(message, t);
        this.errorCode = errorCode;
    }

    public CASRuntimeException(CrossAssetServerErrorCode errorCode, Throwable t) {
        super(t);
        this.errorCode = errorCode;
    }

    public CASRuntimeException(CrossAssetServerErrorCode errorCode, String message, Throwable t, boolean enableSuppression, boolean writableStackTrace) {
        super(message, t, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public CrossAssetServerErrorCode getErrorCode() {
        return errorCode;
    }
}
