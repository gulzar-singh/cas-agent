//************************************************************************
// Copyright (c) 2011-2012 Numerix LLC.  All rights reserved.
// This software comprises valuable trade secrets and may be used,
// copied, transmitted, stored, and distributed only in accordance
// with the terms of a written license or trial agreement and with the
// inclusion of this copyright notice.
//************************************************************************

package com.numerix.cas.iface;

import com.numerix.cas.data.ServerResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.thrift.TException;

/**
 * An enumeration of error codes, and methods to generate ServerResult objects
 * when those errors occur.
 */
@Slf4j
public enum CrossAssetServerErrorCode
{
  /**
   * GeneralError: For otherwise unknown error cases
   */
  GeneralError(100, "General Error"),
  
  /**
   * CacheCommitError: When a commit to a cache fails
   */
  CacheCommitError(200, "Cache Commit Error"),
  
  /**
   * DuplicateError: Two or more duplicates items found when not allowed
   */
  DuplicateError(300, "Duplicate Error"),
  
  /**
   * ProcessXMLError: Error while reading or generating an XML file
   */
  ProcessXMLError(400, "XML File Processing Error"),
  
  /**
   * UnknownItemError: Error while trying to access an item that should exist
   */
  UnknownItemError(500, "Unknown Item Error"),
  
  /**
   * NullArgumentError: Error due to a null value passed in to an API method
   */
  NullArgumentError(600, "Null Argument Error"),
  
  /**
   * EmptyCalculateRequestError: Calculate request had zero trades or markets identified
   */
  EmptyCalculateRequestError(700, "Empty Calculate Request Error"),
  
  /**
   * NetworkError: Error attempting to use message broker to send a message
   */
  NetworkError(800, "Network Error"),
  
  /**
   * IdAllocationError: Error while trying to allocate new IDs for objects
   */
  IdAllocationError(900, "ID Allocation Error"),
  
  /**
   * PlugInSpecificationError: Error with the definition of input data to activate a plugin
   */
  PlugInSpecificationError(1000, "Plug-in Specification Error"),

  /**
   * AuthorizationError: Authorization or authentication error
   */
  AuthorizationError(1100, "User Not Authorized to Access Server or Perform Operation"),

  /**
   * AuthorizationMaxConcurrentUser: Maximum number of concurrent user exceeded
   */
  AuthorizationMaxConcurrentUserError(1110, "Maximum number of concurrent user exceeded"),
  
  /**
   * AuthorizationUserLockedoutError: User is locked out of CAS.
   */
  AuthorizationUserLockedoutError(1120, "User locked out"),
  
  /**
   * ComputeNodeExceededError: Attempting to use more compute nodes than allowed
   */
  ComputeNodeExceededError(1200, "Number of Compute Nodes Exceeds Amount Specified in Numerix License"),

  /**
   * GlobalInfoSetupError: Error while trying to set up global info for a calculation request
   */
  GlobalInfoSetupError(1300, "Unable to Determine Global Information for Calculation"),

  /**
   * NoTemplatesError: Error due to a lack of loaded templates
   */
  NoTemplatesError(1400, "No Templates Defined Error"),
  
  /**
   * UnknownApiExtensionError: Error due trying to call an unknown API extension
   */
  UnknownApiExtensionError(1500, "Unknown API Extension"),
  
  /**
   * GeneralApiExtensionError: Error occurring while trying to run an API extension method
   */
  GeneralApiExtensionError(1600, "API Extension Error"),
  
  /**
   * Job retry limit exceeded error.
   */
  OverRetryLimitError(1700, "Over Retry Limit"),

  /**
   * Number of temp objects in a calc request is exceeded.
   */
  MaxTempObjectError(1800, "Maximum number of temporary object exceeded"),
  
  /**
   * General parameter error.
   */
  ParameterError(1900, "Parameter error"),

  /**
   * Invalid argument error.
   */
  InvalidArgumentError(2000, "Invalid Argument Error"),

  /**
   * IO error.
   */
  IOError(2100, "IO Error"),

  /**
   * Insufficient Memory Error.
   */
  InsufficientMemoryError(2200, "Insufficient Memory Error"),

  /**
   * Insufficient Memory Error.
   */
  InvalidOperationError(2300, "Invalid Operation Error")
  ;

  //===========================================================================
  // Public static methods
  //===========================================================================
  
  /**
   * Log a general error and create a ServerResult for this case
   */
  public static ServerResult getGeneralError(Throwable t)
  {
    return new ServerResult(false, createLogMessage(GeneralError, t));
  }

  /**
   * Log a cache commit error and create a ServerResult for this case
   */
  public static ServerResult getCacheCommitError(Throwable t)
  {
    return new ServerResult(false, createLogMessage(CacheCommitError, t));
  }
  
  /**
   * Log a duplicate item error and create a ServerResult for this case
   */
  public static ServerResult getDuplicateError(String tname, Object name)
  {
    return new ServerResult(false, createLogMessage(DuplicateError,
            String.format("%s - %s", tname, (name != null ? name.toString() : ""))));
  }
  
  /**
   * Log a process XML error and create a ServerResult for this case
   */
  public static ServerResult getProcessXMLError(Throwable t)
  {
    return new ServerResult(false, createLogMessage(ProcessXMLError, t));
  }
  
  /**
   * Log an unknown item error and create a ServerResult for this case
   */
  public static ServerResult getUnknownItemError(String typeName, String id)
  {
    return new ServerResult(false, createLogMessage(UnknownItemError,
            String.format("Could not find %s %s", typeName, (id != null ? id : "<null>"))));
  }

  /**
   * Log a null argument error and create a ServerResult for this case
   */
  public static ServerResult getNullArgumentError(String argName)
  {
    return new ServerResult(false, createLogMessage(NullArgumentError,
            String.format("Null value specified for %s", (argName != null ? argName : "<unknown>"))));
  }

  /**
   * Log an empty calculate request error and create a ServerResult for this case
   */
  public static ServerResult getEmptyCalculateRequestError()
  {
    return new ServerResult(false, createLogMessage(EmptyCalculateRequestError,
            String.format("The calculate request refers to zero trades or markets")));
  }
  
  /**
   * Log a network error and create a ServerResult for this case
   */
  public static ServerResult getNetworkError(Throwable t)
  {
    return new ServerResult(false, createLogMessage(NetworkError, t));
  }
    
  /**
   * Log an ID allocation error and create a ServerResult for this case
   */
  public static ServerResult getIdAllocationError(Throwable t)
  {
    return new ServerResult(false, createLogMessage(IdAllocationError, t));
  }
  
  /**
   * Log a Plugin Specification Error and create a ServerResult for this case
   */
  public static ServerResult getPlugInSpecificationError(String msg)
  {
    return new ServerResult(false, createLogMessage(PlugInSpecificationError, msg));
  }

  /**
   * Log an authorization error and create a ServerResult for this case
   */
  public static ServerResult getAuthorizationError(String msg)
  {
    return new ServerResult(false, createLogMessage(AuthorizationError, msg));
  }

  /**
   * Log a ConcurrentUserError and create a TException.
   * @param msg
   * @return 
   */
  public static TException getAuthorizationMaxConcurrentUserError(String msg)
  {
    String excpMsg = createLogMessage(AuthorizationMaxConcurrentUserError, msg);
    return new TException(excpMsg);
  }
  
  /**
   * Log an UserLockedoutError and create a TException.
   * @param msg
   * @return 
   */
  public static TException getAuthorizationUserLockedoutError(String msg)
  {
    String excpMsg = createLogMessage(AuthorizationUserLockedoutError, msg);
    return new TException(excpMsg);
  }
  
  /**
   * Log an authorization error and create a ServerResult for this case
   */
  public static ServerResult getComputeNodeExceededError(String msg)
  {
    return new ServerResult(false, createLogMessage(ComputeNodeExceededError, msg));
  }

  /**
   * Log an error setting up a calculation due to global info generation problems
   * and create a ServerResult for this case
   */
  public static ServerResult getGlobalInfoSetupError(Throwable t)
  {
    return new ServerResult(false, createLogMessage(GlobalInfoSetupError, t));
  }
  
  /**
   * Log an error about having no templates available
   */
  public static ServerResult getNoTemplatesError(String msg)
  {
    return new ServerResult(false, createLogMessage(NoTemplatesError, msg));
  }
    
  /**
   * Log an error about an unknown API extension
   */
  public static ServerResult getUnknownApiExtensionError(String name)
  {
    String n = (name != null ? name : "");
    return new ServerResult(false, createLogMessage(UnknownApiExtensionError, n));
  }

  /**
   * Log an error about having no templates available
   */
  public static ServerResult getGeneralApiExtensionError(String name, String msg)
  {
    String n = (name != null ? name : "");
    String m = (msg != null ? msg : "");
    return new ServerResult(false, createLogMessage(GeneralApiExtensionError,
            String.format("%s: %s", n, m)));
  }
  
  /**
   * Return the overRetryLimit error and log this error using the task Id given.
   * @param id
   * @return 
   */
  public static ServerResult getOverRetryLimitError(String id)
  {
    return new ServerResult(false, createLogMessage(OverRetryLimitError,
            String.format("JobStatus ID = %s", id)));
  }  
  
  /**
   * Log a max temp object error
   * @param maxCount
   * @return 
   */
  public static ServerResult getMaxTempObjectError(int maxCount)
  {
    return new ServerResult(false, createLogMessage(MaxTempObjectError,
            String.format("Maximum count = %s", maxCount)));
  }
  
  /**
   * Log a parameter error.
   */
  public static ServerResult getParameterError(String msg)
  {
    return new ServerResult(false, createLogMessage(ParameterError, msg));
  }

  /**
   * Log invalid argument error.
   *
   * @param msg message to be processed
   * @return server result with processed error message
   */
  public static ServerResult getInvalidArgumentError(String msg) {
    return new ServerResult(false, createLogMessage(InvalidArgumentError, msg));
  }

  /**
   * Log IO error.
   *
   * @param msg message to be processed
   * @return server result with processed error message
   */
  public static ServerResult getIOError(String msg) {
    return new ServerResult(false, createLogMessage(IOError, msg));
  }

  /**
   * Log Insufficient Memory Error.
   *
   * @param msg message to be processed
   * @return server result with processed error message
   */
  public static ServerResult getInsufficientMemoryError(String msg) {
    return new ServerResult(false, createLogMessage(InsufficientMemoryError, msg));
  }

  /**
   * Log Invalid Operation Error.
   *
   * @param msg message to be processed
   * @return server result with processed error message
   */
  public static ServerResult getInvalidOperationError(String msg) {
    return new ServerResult(false, createLogMessage(InvalidOperationError, msg));
  }

  //===========================================================================
  // Public methods
  //===========================================================================
  
  /**
   * Check to see if the provide error message string is describing this
   * particular type of error, by seeing if the message begins with the
   * relevant prefix
   * @param msg Message to check
   * @return True if the message starts with the same prefix, false if not
   */
  public boolean isError(String msg)
  {
    String prefix = String.format("CAS:%d:", code_m);
    return (msg != null && msg.startsWith(prefix));
  }
  
  //===========================================================================
  // Private methods
  //===========================================================================
  
  /**
   * Private constructor
   */
  private CrossAssetServerErrorCode(int code, String name)
  {
    code_m = code;
    name_m = name;
  }

  /**
   * Generate a message to use for an error for the specified code, and an exception
   */
  private static String createLogMessage(CrossAssetServerErrorCode code, Throwable t)
  {
    // Create the error message string
    String errMsg = (t != null ? t.getMessage() : null);
    String msg = String.format("CAS:%d:%s:%s", code.code_m, code.name_m,
      (errMsg != null ? errMsg : "Application error"));

    // Log the error as well
    log.error(msg, t);

    return msg;
  }

  /**
   * Generate a message to use for an error for the specified code, and a message
   */
  private static String createLogMessage(CrossAssetServerErrorCode code, String m)
  {
    // Create the error message string
    String msg = String.format("CAS:%d:%s:%s", code.code_m, code.name_m, m);
    
    // Log the error as well
    log.error(msg);

    return msg;
  }

  public int getCode() {
    return code_m;
  }

  public String getName() {
    return name_m;
  }

  //===========================================================================
  // Private data
  //===========================================================================

  private int code_m;
  private String name_m;
}
