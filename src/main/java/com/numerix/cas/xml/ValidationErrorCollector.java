/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.numerix.cas.xml;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dfeinber
 */
public class ValidationErrorCollector implements ValidationEventHandler {

    /** List of validation events (with validation errors). */
    private List<ValidationEvent> validationEvents = new ArrayList<ValidationEvent>();

    @Override
    public boolean handleEvent(ValidationEvent validationEvent) { 
        // record the validation error
        validationEvents.add(validationEvent);
        // let validation continue
        return true;
    }
    
    /**
     * Get the collection of validation events
     * @return the collection
     */
    public List<ValidationEvent> getValidationEvents()
    {
      return validationEvents;
    }

    public boolean hasEvents()
    {
      return validationEvents.size() > 0;
    }
    
    /**
     * Compose an error message from a collection of validation events
     * @param events
     * @return 
     */
    public String buildValidationMessage()
    {
      StringBuilder errorMessage = new StringBuilder();
      for (ValidationEvent validationEvent : validationEvents) 
      {
        int line = validationEvent.getLocator().getLineNumber();
        int position = validationEvent.getLocator().getColumnNumber();
        String message = validationEvent.getMessage();

        errorMessage.append("Error ").append(message).append(" occured at line ").append(line).append(", position ").append(position).append("\n");
      }

      return errorMessage.toString();
    }    
}
