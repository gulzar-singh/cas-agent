//************************************************************************
// Copyright (c) 2011-2012 Numerix LLC.  All rights reserved.
// This software comprises valuable trade secrets and may be used,
// copied, transmitted, stored, and distributed only in accordance
// with the terms of a written license or trial agreement and with the
// inclusion of this copyright notice.
//************************************************************************

package com.numerix.cas.xml;

import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.input.BOMInputStream;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.net.URISyntaxException;

public class CASXMLUtils
{
  public CASXMLUtils(Class loaderClass, String className, String resourceName)
  {
    loaderClass_m = loaderClass;
    className_m = className;
    resourceName_m = resourceName;
  }


  /**
   * Marshal a CAS object to a CAS XML string.
   * @param cas the JAXB CAS object
   * @return The XML string as <CAS><\CAS>
   */
  public String marshaltoString(Object cas) throws Exception
  {
    StringWriter sw = new StringWriter();

    getMarshaller().marshal(cas, sw);
    sw.close();

    return sw.toString();
  }

  /**
   * Marshal a object to a XML file.
   * @param cas the JAXB CAS object
   * @param filepath the file path to create
   */
  public void marshaltoFile(Object cas, String filepath) throws JAXBException, SAXException, FileNotFoundException, IOException
  {  
    FileOutputStream file = new FileOutputStream(filepath);
    try {
      getMarshaller().marshal(cas, file);
    }
    finally {
      file.close();
    }
  }

  /**
   * Unmarshal a XML string to a object.
   * @param str the XML string
   * @return The JAXB object as defined by loaderClass, className,resourceName
   */
  public Object unmarshalFromString(String str) throws Exception
  {
    ValidationErrorCollector errorCollector = new ValidationErrorCollector();
    Unmarshaller unmarshaller = getUnmarshaller();
    unmarshaller.setEventHandler(errorCollector);

    // Check to see if the string has a leading BOM character for UTF_32, and if so,
    // remove it because it causes problems
    while (str != null && str.length() > 0 &&
          (str.startsWith("\u0000\uFEFF") || str.startsWith("\uFFFE\u0000")) )
    {
      str = str.substring(2);
    }

    //This assume the UTF_8 strings doesn't have BOM chars EF BB BF since that is
    //1.5 character and can be remove using substring().

    // Check to see if the string has a leading BOM character for UTF_16, and if so,
    // remove it because it causes problems
    while (str != null && str.length() > 0 && 
          (str.startsWith("\uFEFF") || str.startsWith("\uFFFE")) )
    {
      str = str.substring(1);
    }

    Object obj = unmarshaller.unmarshal(new StringReader(str));

    if (errorCollector.hasEvents())
    {
      String errorMessage = errorCollector.buildValidationMessage();
      throw new Exception(errorMessage);
    }
    return obj;
  }

  /**
   * Unmarshal xml stream to a object.
   * @param inputStream the XML stream. This stream need to take care of BOM char
   * @return The JAXB object as defined by loaderClass, className,resourceName
   */
  public Object unmarshalFromStream(InputStream inputStream) throws Exception
  {
    Object obj = null;

    ValidationErrorCollector errorCollector = new ValidationErrorCollector();
    Unmarshaller unmarshaller = getUnmarshaller();
    unmarshaller.setEventHandler(errorCollector);

    obj = unmarshaller.unmarshal(new InputStreamReader(inputStream));

    if (errorCollector.hasEvents())
    {
      String errorMessage = errorCollector.buildValidationMessage();
      throw new Exception(errorMessage);
    }

    return obj;
  }

  /**
   * Unmarshal  XML file into object.
   * @param filepath to the xml file
   * @return The JAXB object as defined by loaderClass, className,resourceName
   */
  public Object unmarshalFromFile(String filepath) throws Exception
  {
    File file = new File(filepath);
    return unmarshalFromFile(file);
  }

  /**
   * Unmarshal  XML file into object.
   * @param fp File object
   * @return The JAXB object as defined by loaderClass, className,resourceName
   */
  public Object unmarshalFromFile(File fp) throws Exception
  {
    FileInputStream fis = new FileInputStream(fp);

    //create an input stream that excludes all BOM markers.
    BOMInputStream bomStream = new BOMInputStream(fis, ByteOrderMark.UTF_8,
                                ByteOrderMark.UTF_32LE, ByteOrderMark.UTF_32BE,
                                ByteOrderMark.UTF_16LE, ByteOrderMark.UTF_16BE);
    return unmarshalFromStream(bomStream);
  }


  /**
   * Get lazy initialized marshaller
   */
  private Marshaller getMarshaller() throws JAXBException, SAXException
  {
    Marshaller marshaller = getJaxbContext().createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    return marshaller;
  }

  /**
   * Get lazy initialized un-marshaller
   */
  private Unmarshaller getUnmarshaller() throws JAXBException, SAXException, URISyntaxException
  {
    Unmarshaller unmarshaller = getJaxbContext().createUnmarshaller();
    unmarshaller.setSchema(getSchema());

    return unmarshaller;
  }

  /**
   * Gets jaxb context
   * @return the context
   */
  private JAXBContext getJaxbContext() throws JAXBException
  {
    if (jaxbContext_s == null)
    {
      ClassLoader loader = loaderClass_m.getClassLoader();
      jaxbContext_s = JAXBContext.newInstance(className_m, loader);
    }

    return jaxbContext_s;
  }

  /**
   * Get lazy initialized xml schema
   * @return schema object
   */
  private Schema getSchema() throws SAXException, URISyntaxException
  {
    if (schema_s == null)
    {
      SchemaFactory sf = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
      ClassLoader loader = loaderClass_m.getClassLoader();
      java.net.URL resource = loader.getResource(resourceName_m);

      schema_s = sf.newSchema(resource);
    }

    return schema_s;
  }

  /*****************************************************
   * Member variables
   *****************************************************/
  private Class loaderClass_m;
  private String className_m;
  private String resourceName_m;

  // The jaxb context
  private JAXBContext jaxbContext_s;

  // The schema
  private Schema schema_s;
}
